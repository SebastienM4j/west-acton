West Acton
==========

Escape Game for Apologic.

_This project is no longer maintained and the escape game was not done... :-/_

Game
----

### Scenario

The three system admins and me follow an formation on IT security. The trainer comes from Paris with his material.
At noon, we gonna eat. System admins with the trainer (it seems to me) go to the restaurant.

I go to eat on my side, but I realize that I forgot my glass in the room...
When i go back to look for it, i see that several laptop have disappeared.
There is only one left, that of Ludo, but it seems to be locked by a virus.

It's our last access to the productions platforms !

When i'm going out to call the police, i meet a group of colleagues. I propose to them to try to unlock the computer.

### Goal

Find the passphrase by elucidating the different puzzles and unclok the false virus before the time is up.

Usage
-----

*Requires JDK 8+*

Launch directly by `./gradlew bootRun`.

Or build with `./gradlew build` and deploy the generated war (located in `build/libs`) on a servlet container,
or launch it by `java -jar build/libs/west-acton-{version}.war`.

By [default](src/main/resources/application.yml) there is a countdown of 5 seconds and the game is of 45 minutes. To override it,
use one of possibilities offered by [Spring Boot externalized configuration](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html).

For example through Gradle `./gradlew bootRun -Pargs="--west-acton.start-countdown=0 --west-acton.game-duration=60"`,
or througt Java `java -jar build/libs/west-acton-{version}.war --west-acton.start-countdown=0 --west-acton.game-duration=60`.




