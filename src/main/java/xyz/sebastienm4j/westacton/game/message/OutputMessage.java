package xyz.sebastienm4j.westacton.game.message;

/**
 * Web-socket message for console output text.
 */
public class OutputMessage implements WebSocketMessage
{
    private boolean newLine;
    private String text;


    /**
     * New output message with text and on new line.
     *
     * @param text Text to output
     */
    public OutputMessage(String text)
    {
        this.newLine = true;
        this.text = text;
    }

    /**
     * New output message with text.
     *
     * @param newLine Print on new line or not
     * @param text Text to output
     */
    public OutputMessage(boolean newLine, String text)
    {
        this.newLine = newLine;
        this.text = text;
    }


    public boolean isNewLine() {
        return newLine;
    }

    public void setNewLine(boolean newLine) {
        this.newLine = newLine;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
