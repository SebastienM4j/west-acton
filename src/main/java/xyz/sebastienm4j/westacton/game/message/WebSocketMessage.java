package xyz.sebastienm4j.westacton.game.message;

/**
 * Marker interface that's indicates an implementation
 * of web-socket message payload.
 */
public interface WebSocketMessage
{

}
