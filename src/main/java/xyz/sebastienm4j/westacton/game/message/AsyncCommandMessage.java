package xyz.sebastienm4j.westacton.game.message;

/**
 * Web-socket asynchronous message for console command.
 */
public class AsyncCommandMessage extends CommandMessage implements WebSocketAsyncMessage
{
    private int millisDelay;


    /**
     * New input command message to simulate
     * typed command, potentially sent asynchronously.
     *
     * @param command Text of the typed command
     * @param millisDelay Delay in milliseconds
     */
    public AsyncCommandMessage(String command, int millisDelay)
    {
        super(command);
        this.millisDelay = millisDelay;
    }

    /**
     * New input command message,
     * potentially sent asynchronously.
     *
     * @param passphraseInput if command prompt is for input passphrase
     * @param millisDelay Delay in milliseconds
     */
    public AsyncCommandMessage(boolean passphraseInput, int millisDelay)
    {
        super(passphraseInput);
        this.millisDelay = millisDelay;
    }


    @Override
    public int getMillisDelay() {
        return millisDelay;
    }

    public void setMillisDelay(int millisDelay) {
        this.millisDelay = millisDelay;
    }

}
