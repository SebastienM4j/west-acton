package xyz.sebastienm4j.westacton.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Configuration properties of the application.
 */
@Component
@ConfigurationProperties("west-acton")
@Validated
public class WestActonConfigProperties
{
    /**
     * Duration in minutes of the escape game
     */
    @Min(1)
    @Max(60)
    private int gameDuration;

    /**
     * Seconds before the game starts
     */
    @Min(0)
    @Max(30)
    private int startCountdown;

    /**
     * List of BCrypt hash of valid answers
     */
    @NotNull
    @Size(min=1)
    private List<String> validEncryptedAnswers;


    public int getGameDuration() {
        return gameDuration;
    }

    public void setGameDuration(int gameDuration) {
        this.gameDuration = gameDuration;
    }

    public int getStartCountdown() {
        return startCountdown;
    }

    public void setStartCountdown(int startCountdown) {
        this.startCountdown = startCountdown;
    }

    public List<String> getValidEncryptedAnswers() {
        return validEncryptedAnswers;
    }

    public void setValidEncryptedAnswers(List<String> validEncryptedAnswers) {
        this.validEncryptedAnswers = validEncryptedAnswers;
    }
}
