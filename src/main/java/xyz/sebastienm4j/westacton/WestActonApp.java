package xyz.sebastienm4j.westacton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import java.util.HashMap;
import java.util.Map;

/**
 * Entry point of the application.
 */
@SpringBootApplication
public class WestActonApp extends SpringBootServletInitializer
{
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder)
    {
        builder.sources(WestActonApp.class)
               .application().setDefaultProperties(defaultProperties());
        return builder;
    }

    public static void main(String[] args)
    {
        SpringApplication application = new SpringApplication(WestActonApp.class);
        application.setDefaultProperties(defaultProperties());
        application.run(args);
    }

    private static Map<String, Object> defaultProperties()
    {
        Map<String, Object> defaultProperties = new HashMap<>();
        defaultProperties.put("server.port", "19180");
        return defaultProperties;
    }
}
