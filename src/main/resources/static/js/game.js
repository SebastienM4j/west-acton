/*
 * Declare web-socket client
 */

var socket = new SockJS("./stomp");
var client = Stomp.over(socket);


/*
 * DOM manipulation for console
 */

var appendCommand = function(command)
{
    $('#console').append('<br/>');
    $('#console').append('<span class="userhost">root@apologic-pc021</span><span class="sep">:</span>');
    $('#console').append('<span class="dir">/usr/local/bin</span><span class="sep">$</span>&nbsp;');
    $('#console').append('<span class="input">'+command+'</span>');
};

var appendOutput = function(newLine, output)
{
    if(newLine) {
        $('#console').append('<br/>');
    }
    $('#console').append('<span class="output">'+output+'</span>');
};

var onInput = function()
{
    var body = { passphrase: $('#passphrase').val() };
    client.send("/app/passphrase", {}, JSON.stringify(body));
    return false;
};


/*
 * Messages listeners
 */

var stateTopicListener = function(message)
{
    if(message.command === "MESSAGE" && message.body)
    {
        var gameSession = JSON.parse(message.body);
        var countdownEnd = undefined;
        var format = undefined;

        switch(gameSession.state)
        {
            case 'STARTING':
                countdownEnd = moment.utc(gameSession.gameBeginning).toDate();
                format = "%S";
                break;

            case 'IN_PROGRESS':
                countdownEnd = moment.utc(gameSession.gameMaximumEnd).toDate();
                format = "%M:%S";
                break;

            case 'GAME_WON':
                try {
                    $('#clock').countdown('stop');
                } catch(e) {
                    var maximumEnd = moment.utc(gameSession.gameMaximumEnd);
                    var end = moment.utc(gameSession.gameEnd);
                    var diff = moment.utc(maximumEnd.diff(end)).format("mm:ss");
                    $('#clock').append(diff);
                }
                $('#clock').removeClass('clock');
                $('#clock').addClass('clock-success');
                break;

            case 'GAME_OVER':
                try {
                    $('#clock').countdown('stop');
                } catch(e) {
                    // nothing to do
                }
                $('#clock').html("00:00");
                $('#clock').removeClass('clock');
                $('#clock').addClass('clock-over');
                break;
        }

        if(countdownEnd !== undefined)
        {
            $('#clock').countdown(countdownEnd).on('update.countdown', function(event) {
                $(this).html(event.strftime(format));
            });
        }
    }
};

var commandTopicListener = function(message)
{
    if(message.command === "MESSAGE" && message.body)
    {
        var command = JSON.parse(message.body);

        if(command.command !== undefined && command.command !== null) {
            $("#inputForm").hide();
            appendCommand(command.command);
        }

        if(command.passphraseInput) {
            $("#passphrase").val("");
            $("#inputForm").show();
            $("#passphrase").focus();
        }
    }
};

var outputTopicListener = function(message)
{
    if(message.command === "MESSAGE" && message.body)
    {
        var output = JSON.parse(message.body);
        appendOutput(output.newLine, output.text);
    }
};


/*
 * Server web-socket connection
 */

var onConnectSuccess = function()
{
    $("#inputForm").hide();

    client.subscribe("/topic/state", stateTopicListener);
    client.subscribe("/topic/command", commandTopicListener);
    client.subscribe("/topic/output", outputTopicListener);

    appendCommand("./crypt84Zio.sh");
    client.send("/app/startup", {});
};

var onConnectError = function(error)
{
    appendOutput(true, "Connection to server failed due to");
    appendOutput(true, error.headers.message);
};

client.connect({}, onConnectSuccess, onConnectError);
